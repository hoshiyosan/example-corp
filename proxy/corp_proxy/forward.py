import logging
from fastapi import Response, Request, HTTPException, status
from corp_proxy.matchers import Service

import aiohttp
import json


def rewrite_url_path(url: str, path: str):
    print('rewrite', url, path)
    return url.rstrip('/') + '/' + path.lstrip('/')


async def forward_request(service: Service, request: Request) -> Response:
    if service is None:
        raise HTTPException(status.HTTP_503_SERVICE_UNAVAILABLE, detail="Service does not exists / is not available")
        
    request_data = {
        "url": rewrite_url_path(service.url, request.path_params["path"]),
        "method": request.method,
        "headers": dict(request.headers),
        "data": await request.body()
    }
    
    async with aiohttp.ClientSession() as session:
        async with session.request(**request_data) as resp:
            response = Response(await resp.read())
            response.headers.update(dict(resp.headers))
            response.status_code = resp.status
    
    return response
