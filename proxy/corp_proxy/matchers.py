import abc
from dataclasses import dataclass
import yaml
from typing import Dict, List, Optional

class BaseMatcher(abc.ABC):
    def __init__(self, options):
        self.validate(options)
        self.options = options
    
    def validate(self, options):
        ...
        

class MatcherOptions:
    method: Optional[str] = None
    prefix: Optional[str]


class Matcher(BaseMatcher):
    def validate(self, options):
        ...
        
from pydantic import BaseModel, Field

class ServiceConfig(BaseModel):
    code: str
    url: str
    middlewares: List[str] = Field(default_factory=list)
    
class Config(BaseModel):
    services: List[ServiceConfig] = Field(default_factory=list)


class Middleware:
    ...
from pydantic import parse_obj_as
@dataclass
class Service:
    code: str
    url: str
    middlewares: List[Middleware]
    
    @classmethod
    def from_config(cls, config: ServiceConfig) -> "Service":
        return Service(url=config.url, code=config.code, middlewares=[])

class SuperMatcher:
    def __init__(self):
        self.services: Dict[str, Service] = {}
        
    def get_service(self, service_code: str) -> Service:
        return self.services.get(service_code)
    
    @classmethod
    def from_settings(cls, filename: str):
        with open(filename, 'r') as settings_file:
            data = yaml.safe_load(settings_file)
        config = parse_obj_as(Config, data)
        
        super_matcher = cls()

        for service_config in config.services:
            super_matcher.services[service_config.code] = Service.from_config(service_config)
            
        return super_matcher
