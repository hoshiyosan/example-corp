from fastapi import Request

from corp_proxy.server import ASGIServer
from corp_proxy.matchers import Service, SuperMatcher
from corp_proxy.forward import forward_request

app = ASGIServer()
super_matcher = SuperMatcher.from_settings("../settings.yaml")

@app.api_route("/gateway/{service_code}/{path:path}", methods=('GET', 'PUT', 'POST', 'DELETE'))
async def forward_request_endpoints(request: Request, service_code: str, path: str):
    service = super_matcher.get_service(service_code)
    return await forward_request(service, request)
